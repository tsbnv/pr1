#include "mydeque.h"
#include "my_assert.h"

///размер очереди
size_t deq_size(const MyDeque &deq) {
    return deq.size;
}

///первый элемент очереди
const deque_t &deq_front(const MyDeque &deq) {
    //Для проверки условий, при которых операция может выполняться следует писать assert
    assert(deq.size > 0);
    return deq.data[deq.head];
}

///последний элемент очереди
const deque_t &deq_back(const MyDeque &deq) {
    assert(deq.size > 0);
    return deq.data[deq.tail];
}

///Очистить очередь
void deq_clear(MyDeque &deq) {
    deq.head = deq.tail;
    deq.size = 0;
}

///вставить элемент в конец очереди
void deq_push_back(MyDeque &deq, deque_t elem) {
    assert(deq.size < 20);
    deq.size++;

    if (deq.size == 1){
        deq.data[deq.tail] = elem;
        return;
    }

    deq.tail = (deq.tail + 1) % deq_data_size;
    if (deq.tail == deq.head)
        deq.head = (deq.head + 1) % deq_data_size;
    deq.data[deq.tail] = elem;
}

///вставить элемент в начало очереди
void deq_push_front(MyDeque &deq, deque_t elem) {
    assert(deq.size < 20);
    deq.size++;

    if (deq.size == 1){
        deq.data[deq.head] = elem;
        return;
    }

    deq.head = (deq_data_size + deq.head - 1) % deq_data_size;
    if (deq.head == deq.tail)
        deq.tail = (deq_data_size + deq.tail - 1) % deq_data_size;
    deq.data[deq.head] = elem;
}

///достать элемент с конца очереди
deque_t deq_pop_back(MyDeque &deq) {
    assert(deq.size > 0);
    deq.size--;
    deque_t element = deq.data[deq.tail];

    if (deq.size == 0){
        return element;
    }

    deq.tail = (deq_data_size + deq.tail - 1) % deq_data_size;

    return element;
}

///достать элемент с начала очереди
deque_t deq_pop_front(MyDeque &deq) {
    assert(deq.size > 0);
    deq.size--;
    deque_t element = deq.data[deq.head];

    if (deq.size == 0){
        return element;
    }

    deq.head = (deq.head + 1) % deq_data_size;

    return element;
}
